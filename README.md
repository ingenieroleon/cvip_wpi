# README #

# aliasComandos

* 20200511 Agrega los comandos de folder al sistema.
* 20200521 Se agregan más comandos, el listado esta en el archivo: /var/www/cvip_wpi/.bash_aliases
* 20210323 Automatiza la copia del archivo alias.
* 20220121 Se arregla estado de permisos si esta en root o ubuntu.
* 20220129 se independiza gitUpdate.
* 20220304 Se corre como BIN BASH para que los comandos queden insertados en el usuario.
* 20220304 Se renombra getSize por folderSize (más claro).

# exportDB_CLI.sh
* 20220311 se crea funcion que exporta base de datos por CLI.

# importDB_CLI.sh
* 20220311 se crea funcion que importa base de datos por CLI.

# convertWebP.sh

* Convierte todos los png, jpg, jpeg a webp con la extension que nececita wordops, verifica que no exista el archivo, necesita tener instalado (sudo apt install webp).
* 20210725 Mejorado para funcionar dentro de HTDOCS.
* 20220121 se activa que copie metadatos para mantener perfil de color.

# gitUpdate

* Hace add, commit con fecha y push de un folder git.
* 20220129 Se movio a archivo independiente y se cambia formato de fecha.

# updateWP
* Updates themes and plugins in all wordpress Wordops.
* 20200508.
* Added option to skip theme or plugin update individually.
* 20200511.

# siteInfoAll_WO
* List info of all sites in WO.
* 20200508 Se le agrega una pausa por sitio para tener tiempo de leer.
* 20220128 se actualiza el script para poner lineas divisoras en cada vhost.
* 20220402 se actualiza el script para resumir la información a una línea por cada vhost;

# exportallNGINX_ignoreUploads.sh

* Funciona igual que exportallNGINX.sh a excepción que ignora la carpeta wp-content/uploads/*

# exportwpNGINX_ignoreUploads.sh

* Funciona igual que exportwpNGINX.sh a excepción que ignora la carpeta wp-content/uploads/*

# move_backups_to.sh

* Mueve de la carpeta backup, las carpetas "*-FILE" y "*-DB", a una ubicación especificada o por default descargas.colombiavip.com

# softRestart
* Reinicia PHP y luego recarga nginx, de esta manera se libera CPU y RAM sin perder el log de wordops.

# resumen.sh
* Muestra un resumen de los sitios deshabilitados, total de sitios y un listado ordenado alafabeticamente.
* 20211118 se mejora el orden y los colores.

# exportwpNGINX_CLI.sh

* Exportación de Wp utilizando Wp Cli
* Mejorado para no necesitar sudo y borradas variables inecesarias.

# importwpCliNGINX.sh

* Importación de Wp utilizando Wp Cli

# getSize.sh

* Se obtiene el peso en MB de todas los archivos donde se corre el script.

# gdrivedl.sh

* Permite descargar archivos directamente desde GDriver
* /var/www/cvip_wpi/gdrivedl.sh {ENLACE GDrive} {[RUTA_ARCHIVO]/[NOMBRE_ARCHIVO].[EXTENSION_ARCHIVO]}

# EE4
* Scripts de backups y restore de EasyEngine 4 (revisar para mejorar los de WO).

# TEMPLATES
* Archivos que solo es editar, copiar y pegar en una ruta.

# testdb.php (MOVIDO A TEMPLATES)

* Script de conexión DB sencillo.


# robots_disuadir.txt (MOVIDO A TEMPLATES/ROBOTS)

* Robots para disuadir a los motores de busquedad.
* Nota: reemplazar el nombre del archivo por "robots.txt".

## index.nginx-debian.html

* Plantilla de sitio en construcción, reemplazar en /var/www/html.

## CRONTABS

* Crontabs usados con frecuencia.


## ACTIVATOR
* Script de activación del tema CVIP.
* 20200508.


## SSL
* This folder has two sample files for SSL configuration.
* Mejorado con nombre de TLDR y ruta en conf.
* 20200628.


## mascara.dominio.com
* Masking of a site with nginx.


## REDIR
* Redirect of a site to another with parameters.

## UDB
A db query to update URL.
* Edit file to reflect old and new url.
* Run the query on your database.


## vhosts.conf
* Plantilla de host apache.


## logINFO.txt
* Plantilla de log para script infoALL


## BASH_ALIASES
* Sample of bash aliases file.
* 20210521 actualizado.

-----------------------------------
# LIMPIARPLUGINS

# This is a simple plugin updater.
* run limpiarplugins.sh
* 20190223



# DISABLEFILEEDITINGWP
* run DisableFileEditingWP.sh
* 20190225



# ACTUALIZARCUSTOMPLUGINS

# This is a simple plugin updater.
* run actualizarCustomPlugins.sh
* MUST BE UPDATED: only WPDB added
* 20181009
* Cambio nombre para no confundir con el update normal.
* 20200511



# EXPORTWP__SERVER__

# This is a simple wordpress exporter.
* Put the ewp.sh file on the htdocs folder next to wp-config.php files
* run the file and download generated zip
* Creado exportwpNGINX para ese tipo de server.
* Se puso la opción de exportar DB o ARCHIVOS.
* Se mejora patr no requerir sudo.
* Se simplifica código con la variable FILENAME.
* Se corrige error en TIMESTAMP.
* 20200612.

### What is this repository for? ###

* Creates a backup of database and files of a wordpress, make sure to own file with user that runs the script
* Se añadio version para NGINX
* 20190222.




# EXPORTALL__SERVER__

# This walks all apps on bitnami an run the exporter.
* run the file and download generated zip files.
* Corregido: no necesita sudo para servir.
* 20200610.




# EXPORTALLDATABASES

# This walks all apps on NGINX and export the DB.
* /var/www/cvip_wpi/exportallDBnginx.sh
* run the file and download generated zip files.
* 20180906
* 20200327: se agrega backup de todas.




# EXPORTALLDATABASESJET

# This walks all apps on JETWARE and export the DB.



#IMPORTWP__SERVER__

# This is a wordpress import.
* Put de backup zip file on the htdocs folder
* Put the iwp.sh file on the htdocs folder next to wp-config.php files
* run the file and manually edit perms and vhost

### What is this repository for? ###

* Recovers a Backup created with EWP.
* 20180831




# IMPORTWPNGINX

# This is a wordpress import for NGINX.
* Put the backup zip file on the htdocs folder
* Put the iwp.sh file on the htdocs folder next to wp-config.php files
* run the file and manually edit perms and vhost
* Mejorado para que no necesite sudo.
* Se comenta linea UPDATE wp_posts SET guid para evitar dañar algunos temas.
* 20200628.

### What is this repository for? ###

* Recovers a Backup created with EWP on NGINX.
* 20180930-RENAMED
* 20190401-JOINED WITH UPDATE URL




#INFO

# A simple info php file.
* Put the file in htdocs and access it on browser

### What is this repository for? ###

* Show php system info




#PERMS

# This sets correctly perms on wordpress directory.
* Put the perms.sh file on the htdocs folder next to wp-config.php files
* run the file

### What is this repository for? ###

* Sets permitions recusively on every file and folders of wordpress structure




#SUBIRADRIVE

# This file reads all FOLDERS on ../backup, upload the last drive.
* Backup folder must be located next to cvip_wpi.
* Run the file.
* Script file "gdrive" must be on cvip_wpi folder with a+x perms, exact name strict.


#UPDATEDBURL

# A script based on UDB with automated promt of old and new URL.
* Put this file on wp-content.
* Run file and set the old and new url.

### What is this repository for? ###

* Updates your db to new site
* where clause to avoid WP OPTIONS to delete theme header and footer
* 20190330

### UPDATES NEEDED ###

* NEEDED to update theme options, a clue is this query: SELECT * FROM wp_options WHERE option_name LIKE '%theme_mods_%'




# WPINSTALL

# This is a simple shell installer FOR JETWARE.
* Please move all files on this package to apps folder root
* Take care to add vhost.conf to: "\jet\etc\apache\httpd.conf"
* with the path example:
```
# VHOSTS APPS
Include /jet/app/www/default/vhosts.conf
```
### What is this repository for? ###

* Autoinstall WP on jetware and amazon linux, take care of sudo not used on jetware but probably needed on another linux
* 20180426


# VHOSTS

# File needed for WPI installer, read WPI instructions.
