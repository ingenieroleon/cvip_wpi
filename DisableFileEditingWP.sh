for d in $(ls /var/www/*/wp-config.php)
do
	if  ! grep "DISALLOW_FILE_EDIT" ${d}; then
		echo "//Disable File Editing in the Admin Area of WordPress" >> ${d}
		echo "define( 'DISALLOW_FILE_EDIT', true );" >> ${d}
	fi
	echo "---EXITO ${d} ---\n"
done
