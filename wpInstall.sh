#CORRER PREFERIBLEMENTE VIA SSH YA QUE EN GIT O  VM LA RUTA SE ENREDA PIDO VARIABLES
read -p "Nombre APP/DB/USER:" nombrewp &&
read -p "Clave para DB:" passdb &&
read -p "Clave del usurio root de la DB (Si no lo has seteado corre: mysqladmin -u root password NEWPASSWORD):" rootpasswd &&
#CREO EL VHOSTS
midir=$(pwd) &&
cat <<EOF >> vhosts.conf

#VHOST PARA $nombrewp - include it to HTTPD CONF ON JET: "\jet\etc\apache\httpd.conf"

<VirtualHost *:80>
	ServerName $nombrewp.localhost
	ServerAlias *$nombrewp.*
	DocumentRoot "$midir/$nombrewp"
</VirtualHost>
EOF

read -p "Listo para instalar $nombrewp..." key	&&
#CREO USUARIO Y DB
mysql -uroot -p${rootpasswd} -e "CREATE DATABASE ${nombrewp} /*\!40100 DEFAULT CHARACTER SET utf8 */;" &&
mysql -uroot -p${rootpasswd} -e "CREATE USER ${nombrewp}@localhost IDENTIFIED BY '${passdb}';" &&
mysql -uroot -p${rootpasswd} -e "GRANT ALL PRIVILEGES ON ${nombrewp}.* TO '${nombrewp}'@'localhost';" &&
mysql -uroot -p${rootpasswd} -e "FLUSH PRIVILEGES;" &&
FILE=wp-config.php &&

#BAJO WP y CAMBIO DATOS	
pwd && wget -N -c http://wordpress.org/latest.zip &&
unzip -o latest.zip &&
mv ./wordpress ./$nombrewp -f &&
cd ./$nombrewp &&
cp wp-config-sample.php $FILE &&
sed -i 's/database_name_here/'$nombrewp'/g' $FILE &&
sed -i 's/username_here/'$nombrewp'/g' $FILE &&
sed -i 's/password_here/'$passdb'/g' $FILE &&

#AGREGO EL SINTENAME DINAMICO
LINE="define('WP_SITEURL', 'http://' . \$_SERVER['HTTP_HOST'] . '/');" &&
grep -qF "$LINE" "$FILE" || echo "$LINE" >> "$FILE" &&
LINE="define('WP_HOME', 'http://' . \$_SERVER['HTTP_HOST'] . '/');" &&
grep -qF "$LINE" "$FILE" || echo "$LINE" >> "$FILE" &&


# CAMBIAR PERMISOS
read -p "Owner for WP files:" WP_OWNER && read -p "Group for WP files:" WP_GROUP && echo $WP_OWNER $WP_GROUP && read -p "Press any key to continue..." key &&

# reset to safe defaults
sudo chown -R ${WP_OWNER}:${WP_GROUP} . &&
find . -type d -exec sudo chmod 755 {} \; &&
find . -type f -exec sudo chmod 644 {} \; &&

# allow wordpress to manage wp-config.php (but prevent world access)
chmod 660 ./wp-config.php &&

# allow wordpress to manage .htaccess
#chmod 664 ./.htaccess &&

# allow wordpress to manage wp-content Y REINICIO APACHE PARA QUE AGARRE EL VHOST
find ./wp-content -type d -exec sudo chmod 775 {} \; &&
find ./wp-content -type f -exec sudo chmod 664 {} \; &&	restart