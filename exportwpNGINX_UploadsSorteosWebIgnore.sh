#!/bin/bash
#/************************ //SALVAR DB Y ARCHIVOS**********************/
TIMESTAMP=$(date +"%F")
username=$(grep DB_USER ../wp-config.php | cut -d \' -f 4)
password=$(grep DB_PASSWORD ../wp-config.php | cut -d \' -f 4)
dbName=$(grep DB_NAME ../wp-config.php| cut -d \' -f 4)
FILENAME="${TIMESTAMP}-${dbName}"
echo "----------------------------------------------------"
read -p "Exportar base de datos: " IDB
echo    # (optional) move to a new line
if [[ $IDB =~ ^[YySs]$ ]]
then
	echo "EXPORTANDO ${dbName} MySQL..."
	sudo /usr/bin/mysqldump -u ${username} -p"${password}"  ${dbName} | sudo dd of="${FILENAME}.sql" && echo "DB EXPORTADA CON EXITO!" || echo "ERROR al exportar DB"
fi

echo "----------------------------------------------------"
read -p "Exportar archivos: " IARCHIVOS
echo    # (optional) move to a new line
if [[ $IARCHIVOS =~ ^[YySs]$ ]]
then
	echo compressing
	sudo zip -r "${FILENAME}.zip" "wp-content/uploads" "${FILENAME}.sql" -x "wp-content/uploads/sorteosWeb/*" && echo "ARCHIVOS EXPORTADOS CON EXITO" || echo "ERROR al exportar ARCHIVOS"
fi
