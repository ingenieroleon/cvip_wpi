#/************************ //IMPORTAR DB Y ARCHIVOS NGINX**********************/
nombrebk=$(ls -t *.zip|head -n 1 | cut -d "." -f 1)
echo "NOMBRE BACKUP: ${nombrebk}"

dbUser=$(grep DB_USER ../wp-config.php | cut -d \' -f 4)
dbPass=$(grep DB_PASSWORD ../wp-config.php | cut -d \' -f 4)
dbName=$(grep DB_NAME ../wp-config.php| cut -d \' -f 4)

read -p "Importar archivos (y/n): " IARCHIVOS
echo    # (optional) move to a new line
if [[ $IARCHIVOS =~ ^[Yy]$ ]]
then
    echo DESCOMPRIMIENDO: "${nombrebk}.zip"
	sudo unzip -o  "${nombrebk}.zip"

	echo "CAMBIANDO PERMISOS"
	sudo chmod -R 755 wp-content
	sudo chown -R www-data:www-data wp-content
fi


read -p "Importar base de datos (y/n): " IDB
echo    # (optional) move to a new line
if [[ $IDB =~ ^[Yy]$ ]]
then
	echo Datos de usuario
	echo User: ${dbUser}
	echo Pass: ${dbPass}
	echo DB: ${dbName}
	read -p "¿CONTINUAR CON IMPORTAR: ${nombrebk}.sql? (y/n) " continuar
	echo "----------------------------------------------------"
	
	sudo chown www-data:www-data ${nombrebk}.sql
	
	echo "Importando MySQL ${nombrebk}.sql..."
	sudo wp db import "${nombrebk}.sql" --dbuser="${dbUser}" --dbpass="${dbPass}" --allow-root
fi


read -p "Actualizar URL (y/n): " UDBURL
echo    # (optional) move to a new line
if [[ $UDBURL =~ ^[Yy]$ ]]
then

	siteurl=$(wp option get siteurl --allow-root)
	home=$(wp option get home --allow-root)

	echo "siteurl: ${siteurl}";
	echo "home   : ${home}";

	read -p "NUEVA URL: " newurl

	read -p "¿Confirmar? (y/n) " confirmar
	
	wp option update siteurl "${newurl}" --allow-root
	wp option update home "${newurl}" --allow-root
fi

echo "-----------------------FIN-----------------------------"
