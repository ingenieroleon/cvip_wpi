<?php
//EXT DIR
//echo 'ext_dir: ' . ini_get('extension_dir'). '<br>';
//Your memory limit
echo 'memory_limit: ' . ini_get('memory_limit') . '<br>';
//Your maximum size of post-data (including file uploads)
echo 'post_max_size: ' . ini_get('post_max_size') . '<br>';
//The maximum file size for uploads
echo 'upload_max_filesize: ' . ini_get('upload_max_filesize') . '<br>';
//Maximum runtime for php scripts (in seconds)
echo 'max_execution_time: ' . ini_get('max_execution_time') . '<br>';
//Current error reporting level
echo 'error_reporting: ' . ini_get('error_reporting') . '<br>';
//Are errors displayed?
echo 'display_errors: ' . ini_get('display_errors') . '<br>';
//Will errors be logged?
echo 'log_errors: ' . ini_get('log_errors') . '<br>';
//Where will errors be logged?
echo 'error_log: ' . ini_get('error_log') . '<br>';
//What is the absolute path of this files parent folder
// = the complete path to your wordpress "root folder"
echo 'root: ' . __DIR__ . '<br>';

/**
 * If you are curious to see *a lot* of information about your environment
 * then uncomment this line too:
 */
phpinfo();

/**
 * This should print whatever is in the error log, but it could potentially
 * be huge, so use with caution!
 */
//echo '<pre>' . file_get_contents(ini_get('error_log')) . '</pre><br>';