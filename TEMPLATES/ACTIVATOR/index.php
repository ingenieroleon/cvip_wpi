<?php
$enabled_sites = [
	'desarrollo.colombiavip.com',
	'soendertransport.nl',
	'www.soendertransport.nl', 
	'en.soendertransport.nl',
	'soender.perfektany.com',
];

$variables = null;

if (isset($_POST['home_url']) && in_array($_POST['home_url'], $enabled_sites)){	
	$variables = array(	  
	  'variabledecontrol' => true, # VARIABLE DE ACTIVACION DEL TEMA
	  'cvip_tu'           => $_POST['protocol'].$_POST['home_url'].'/wp-content/themes/cvip_starter', # URL EN NAVEGADOR DEL TEMA
	  'cvip_bt'           => 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css',
	  'cvip_fa'           => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
	  'cvip_bt_j'         => 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js',
	  'cvip_wg'           => '/cvip_widgets.php',
	); 

	echo json_encode($variables);
}
?>

 