server {
	
    server_name subdominio.dominio.com   www.subdominio.dominio.com;
	
    location / {
	 
        proxy_pass http://DIRECION_DEL_SITIO;
		
        #proxy_redirect     off;
        #proxy_set_header   Host $host;
        #proxy_set_header   X-Real-IP $remote_addr;
        #proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
        #proxy_set_header   X-Forwarded-Host $server_name;

    }

}