#/************************ //SALVAR DB Y ARCHIVOS**********************/
### NO USAR SUDO EN ESTE CASO!
username=$(grep DB_USER wp-config.php | cut -d \' -f 4) && password=$(grep DB_PASSWORD wp-config.php | cut -d \' -f 4) && dbName=$(grep DB_NAME wp-config.php| cut -d \' -f 4) && echo "----------------------------------------------------"
echo "Dumping MySQL..."
mysqldump -u ${username} -p"${password}"  ${dbName} > "$(date +%F)-${dbName}.sql" && echo ""
echo "----------------------------------------------------"
echo compressing
zip -r "$(date +%F)-${dbName}.zip" wp-content "$(date +%F)-${dbName}.sql" && echo ""
rm "$(date +%F)-${dbName}.sql"&& echo "Done!"