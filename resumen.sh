tput setaf 1;
echo -n "Disabled sites "
sudo wo site list --disabled | wc -l
sudo wo site list --disabled | sort -n
tput setaf 2;
echo -n "Enabled sites "
sudo wo site list --enabled | wc -l
sudo wo site list --enabled | sort -n
echo -n "Total sites "
sudo wo site list | wc -l
sudo wo site list | sort -n