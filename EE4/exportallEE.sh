#!/bin/bash

ee="/opt/easyengine/sites"
backup="/opt/easyengine/backup"
sites=$(ls $ee)
#year=`date +%Y`
#month=`date +%m`
#day=`date +%d`
date=`date +%Y%m%d`

echo "FECHA: ${date}"
mkdir -p $backup/$date-FILE

for i in $sites
do
	read -p "Backup de $i: " aceptar
	echo    # (optional) move to a new line
if [[ $aceptar =~ ^[Yy]$ ]]
then
		ee shell $i --command="wp db export $i.sql"
		cd $ee/$i/app && sudo zip -r "${date}-${i}.zip" htdocs/wp-content htdocs/${i}.sql 
		#rm $ee/$i/app/htdocs/$i.sql
		mv $ee/$i/app/$date-$i.zip $backup/$date-FILE
fi

done

chown -R www-data:www-data $backup