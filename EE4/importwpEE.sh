#/************************ //IMPORTAR DB Y ARCHIVOS NGINX**********************/
nombrebk=$(ls -t *.zip|head -n 1 | cut -d "." -f 1)
nombreWP=$(basename $(dirname $(dirname $PWD)))
echo "NOMBRE BACKUP: ${nombrebk}"
echo "NOMBRE SITIO WP: ${nombreWP}"


read -p "Importar archivos: " IARCHIVOS
echo    # (optional) move to a new line
if [[ $IARCHIVOS =~ ^[Yy]$ ]]
then
    echo DESCOMPRIMIENDO: "${nombrebk}.zip"
	sudo unzip -o  "${nombrebk}.zip"

	echo "CAMBIANDO PERMISOS"
	sudo chmod -R 755 wp-content ${nombrebk}.sql
	sudo chown -R www-data:www-data wp-content ${nombrebk}.sql
fi


read -p "Importar base de datos: " IDB
echo    # (optional) move to a new line
if [[ $IDB =~ ^[Yy]$ ]]
then
	read -p "CONTINUAR CON IMPORTAR: ${nombrebk}.sql ?" continuar
	echo "----------------------------------------------------"
	echo "Importando MySQL ${nombrebk}.sql..."
	
	sudo ee shell ${nombreWP} --command="wp db import ${nombrebk}.sql"	
fi

read -p "Actualizar URL: " UDBURL
echo    # (optional) move to a new line
if [[ $UDBURL =~ ^[Yy]$ ]]
then

	#URLS actuales
	echo "HOME ACTUAL: "
	sudo ee shell ${nombreWP} --command="wp option get home"
	echo "SITE URL ACTUAL: "
	sudo ee shell ${nombreWP} --command="wp option get siteurl" 
	
	
	read -p "URL ANTIGUA:" oldurl
	read -p "URL NUEVA:" newurl
	
	echo "DATOS-----------------------"

	echo URL ANTIGUA: ${oldurl}
	echo URL NUEVA: ${newurl}
	read -p "Confirmar?" confirmar

	sudo ee shell ${nombreWP} --command="wp option update home '${newurl}'"
	sudo ee shell ${nombreWP} --command="wp option update siteurl '${newurl}'"	
	sudo ee shell ${nombreWP} --command="wp search-replace '${oldurl}' '${newurl}' --skip-columns=guid"
fi

echo "-----------------------FIN-----------------------------"