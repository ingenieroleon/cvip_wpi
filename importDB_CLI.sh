#!/bin/bash
#/************************ //SALVAR DB **********************/
ls *.sql
read -p "SELECCIONA BASE DE DATOS: " FILENAME
#/************************ //EXPORTAR CLI **********************/
wp db import "${FILENAME}" --allow-root

read -p "Actualizar URL (y/n): " UDBURL
echo    # (optional) move to a new line
if [[ $UDBURL =~ ^[Yy]$ ]]
then

	siteurl=$(wp option get siteurl --allow-root)
	home=$(wp option get home --allow-root)

	echo "siteurl: ${siteurl}";
	echo "home   : ${home}";

	read -p "NUEVA URL: " newurl
	
	wp option update siteurl "${newurl}" --allow-root
	wp option update home "${newurl}" --allow-root
	echo "URL ACTUALIZADA";
fi

