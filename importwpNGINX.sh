#/************************ //IMPORTAR DB Y ARCHIVOS NGINX**********************/
nombrebk=$(ls -t *.zip|head -n 1 | cut -d "." -f 1)
echo "NOMBRE BACKUP: ${nombrebk}"

dbUser=$(grep DB_USER ../wp-config.php | cut -d \' -f 4)
dbPass=$(grep DB_PASSWORD ../wp-config.php | cut -d \' -f 4)
dbName=$(grep DB_NAME ../wp-config.php| cut -d \' -f 4)

read -p "Importar archivos: " IARCHIVOS
echo    # (optional) move to a new line
if [[ $IARCHIVOS =~ ^[Yy]$ ]]
then
    echo DESCOMPRIMIENDO: "${nombrebk}.zip"
	sudo unzip -o  "${nombrebk}.zip"

	echo "CAMBIANDO PERMISOS"
	sudo chmod -R 755 wp-content
	sudo chown -R www-data:www-data wp-content
fi


read -p "Importar base de datos: " IDB
echo    # (optional) move to a new line
if [[ $IDB =~ ^[Yy]$ ]]
then
	echo datos de usuario
	echo User: ${dbUser}
	echo Pass: ${dbPass}
	echo DB: ${dbName}
	read -p "CONTINUAR CON IMPORTAR: ${nombrebk}.sql ?" continuar
	echo "----------------------------------------------------"
	echo "Importando MySQL ${nombrebk}.sql..."
	sudo mysql -u ${dbUser} -p"${dbPass}" --one-database ${dbName} < ${nombrebk}.sql
fi

read -p "Actualizar URL: " UDBURL
echo    # (optional) move to a new line
if [[ $UDBURL =~ ^[Yy]$ ]]
then

	username=$(grep DB_USER ../wp-config.php | cut -d \' -f 4) && password=$(grep DB_PASSWORD ../wp-config.php | cut -d \' -f 4) && dbName=$(grep DB_NAME ../wp-config.php| cut -d \' -f 4)
	
	mysql -u ${username} -p"${password}" ${dbName} -e "SELECT option_name, option_value FROM wp_options WHERE option_name = 'home' OR option_name = 'siteurl';"
	
	read -p "URL ANTIGUA:" oldurl
	read -p "URL NUEVA:" newurl
	echo USUARIO: ${username}
	echo CLAVE: ${password}
	echo BASE DE DATOS: ${dbName}
	echo URL ANTIGUA: ${oldurl}
	echo URL NUEVA: ${newurl}
	read -p "Confirmar?" confirmar
	mysql -u ${username} -p"${password}" ${dbName} -e "
	select * from wp_options limit 2;
	SET @oldsite='${oldurl}';
	SET @newsite='${newurl}';
	UPDATE wp_options SET option_value = replace(option_value, @oldsite, @newsite) WHERE option_name = 'home' OR option_name = 'siteurl';
	UPDATE wp_posts SET post_content = replace(post_content, @oldsite, @newsite);
	UPDATE wp_links SET link_url = replace(link_url, @oldsite, @newsite);
	UPDATE wp_postmeta SET meta_value = replace(meta_value, @oldsite, @newsite);
	/* only uncomment next line if you want all your current posts to post to RSS again as new */
	/* UPDATE wp_posts SET guid = replace(guid, @oldsite, @newsite); */
	select * from wp_options limit 5;"

fi

echo "-----------------------FIN-----------------------------"