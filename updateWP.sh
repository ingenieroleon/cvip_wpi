echo "Iniciando"
for d in $(ls /var/www/*/wp-config.php)
do	echo ACTUALIZANDO DE WORDPRESS: $d
	parentdir="$(dirname "$d")/htdocs/"
	echo "Directorio ${parentdir}"
	cd ${parentdir}
read -p "Actualizar temas?: " UTEMAS
echo    # (optional) move to a new line
if [[ $UTEMAS =~ ^[Yy]$ ]]
then
	sudo wp theme update --all --allow-root
fi
read -p "Actualizar plugins?: " UPLUGINS
echo    # (optional) move to a new line
if [[ $UPLUGINS =~ ^[Yy]$ ]]
then
	sudo wp plugin update --all --allow-root
fi	
	
done