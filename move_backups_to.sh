cd "/var/www/backup/"

path=$1
if [  -z "$path" ]; # NO LLEGA VARIABLE $1
then
	path="/var/www/descargas.colombiavip.com/htdocs"
fi

for d in $(ls /var/www/backup)
do	
	prex_files='-FILE'
	prex_DB='-DB'
	
	if [[ "$d" =~ "$prex_files" ]] || [[ "$d" =~ "$prex_DB" ]]; then
		echo "MOVIENDO: ${d}"
		mv $d $path
	fi
	
done 

echo "LISTO!!!"