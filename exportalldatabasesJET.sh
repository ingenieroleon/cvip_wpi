TIMESTAMP=$(date +"%F")
BACKUP_DIR="/jet/app/www/default/backup/${TIMESTAMP}-LOTERIAS"
mkdir -p "$BACKUP_DIR"
cd ${BACKUP_DIR}

echo "

-------------------------------------------------------------
 INICIANDO BACKUP EN: $(pwd)
-------------------------------------------------------------
"
for d in $(ls /jet/app/www/default/*/wp-config.php)
do	echo BACKUP DE WORDPRESS: $d
	username=$(grep DB_USER ${d} | cut -d \' -f 4)
	password=$(grep DB_PASSWORD ${d} | cut -d \' -f 4)
	dbName=$(grep DB_NAME ${d}| cut -d \' -f 4)
	echo "----------------------------------------------------"
	echo "USUARIO: ${username}"
	echo "PASS: ${password}"
	echo "BASE DE DATOS ${dbName}..."
	FILENAME="${TIMESTAMP}-${dbName}"
	#IMPORTANTE: la ruta debe ser completa para evitar que sudo no pueda ejecutarlo
	/jet/bin/mysqldump -u ${username} -p"${password}"  ${dbName} > "${FILENAME}.sql"
done

echo "------COMPRIMIENDO------"
sudo zip -r "${TIMESTAMP}.zip" *.sql
sudo rm *.sql

echo "------CAMBIANDO PERMISOS------"
chmod -R 775 ${BACKUP_DIR}
chown -R ec2-user:ec2-user ${BACKUP_DIR}

echo "LISTO!!!"