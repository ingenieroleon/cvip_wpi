#!/bin/bash
#/************************ //SALVAR DB **********************/
TIMESTAMP=$(date +"%F")
dbName=$(wp config get DB_NAME)
FILENAME="${TIMESTAMP}-${dbName}"
#/************************ //EXPORTAR CLI **********************/
echo "EXPORTANDO ${dbName} usando CLI..."
sudo wp db export "${FILENAME}.sql" --allow-root