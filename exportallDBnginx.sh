TIMESTAMP=$(date +"%F")
BACKUP_DIR="/var/www/backup/$TIMESTAMP-DB"
mkdir -p "$BACKUP_DIR"
cd ${BACKUP_DIR}
echo BACKUP EN: $(pwd)
echo "Iniciando"
for d in $(ls /var/www/*/wp-config.php)
do	echo BACKUP DE WORDPRESS: $d
	username=$(grep DB_USER ${d} | cut -d \' -f 4)
	password=$(grep DB_PASSWORD ${d} | cut -d \' -f 4)
	dbName=$(grep DB_NAME ${d}| cut -d \' -f 4)
	echo "----------------------------------------------------"
	echo "USUARIO: ${username}"
	echo "PASS: ${password}"
	echo "BASE DE DATOS ${dbName}..."
	FILENAME="${TIMESTAMP}-${dbName}"
	mysqldump -u ${username} -p"${password}"  ${dbName} > "${FILENAME}.sql"
	echo "------COMPRIMIENDO------"
	sudo zip -r "${FILENAME}.zip" "${FILENAME}.sql"
	sudo rm "${FILENAME}.sql"
done

##20200327: SE COMENTA PORQUE AHORA NO NECESITAMOS EL UNIFICADO PARA GDRIVE 
#sudo zip -r "${TIMESTAMP}-TODOS.zip" ${BACKUP_DIR} 


echo "------CAMBIANDO PERMISOS------"
chmod -R 775 ${BACKUP_DIR}
chown -R ubuntu:www-data ${BACKUP_DIR}

echo "------REINICIANDO STACK ------" 
sudo su ubuntu -c 'sudo wo stack reload'

echo "LISTO!!!"