#/************************ //ACTUALIZAR URL DB Y NGINX**********************/
# RUN AS SUDO ON NGINX
read -p "URL ANTIGUA:" oldurl
read -p "URL NUEVA:" newurl
username=$(grep DB_USER ../wp-config.php | cut -d \' -f 4) && password=$(grep DB_PASSWORD ../wp-config.php | cut -d \' -f 4) && dbName=$(grep DB_NAME ../wp-config.php| cut -d \' -f 4) && echo "----------------------------------------------------"
echo USUARIO: ${username}
echo CLAVE: ${password}
echo BASE DE DATOS: ${dbName}
echo URL ANTIGUA: ${oldurl}
echo URL NUEVA: ${newurl}
read -p "Confirmar?" confirmar
mysql -u ${username} -p"${password}" ${dbName} -e "
select * from wp_options limit 2;
SET @oldsite='${oldurl}';
SET @newsite='${newurl}';
UPDATE wp_options SET option_value = replace(option_value, @oldsite, @newsite) WHERE option_name = 'home' OR option_name = 'siteurl';
UPDATE wp_posts SET post_content = replace(post_content, @oldsite, @newsite);
UPDATE wp_links SET link_url = replace(link_url, @oldsite, @newsite);
UPDATE wp_postmeta SET meta_value = replace(meta_value, @oldsite, @newsite);
/* only uncomment next line if you want all your current posts to post to RSS again as new 
UPDATE wp_posts SET guid = replace(guid, @oldsite, @newsite); */
select * from wp_options limit 5;"
