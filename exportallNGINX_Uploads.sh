TIMESTAMP=$(date +"%F")
BACKUP_DIR="/var/www/backup/$TIMESTAMP-FILE"
mkdir -p "$BACKUP_DIR"
cd ${BACKUP_DIR}
echo BACKUP EN: $(pwd)
echo "Iniciando"
for d in $(ls /var/www/*/wp-config.php)
do	echo BACKUP DE WORDPRESS: $d
	parentdir="$(dirname "$d")/htdocs/"
	echo "Directorio ${parentdir}"
	cd ${parentdir}
	username=$(grep DB_USER ${d} | cut -d \' -f 4)
	password=$(grep DB_PASSWORD ${d} | cut -d \' -f 4)
	dbName=$(grep DB_NAME ${d}| cut -d \' -f 4)
	echo "----------------------------------------------------"
	echo "USUARIO: ${username}"
	echo "PASS: ${password}"
	echo "BASE DE DATOS ${dbName}..."
	FILENAME="${TIMESTAMP}-${dbName}"
	sudo /usr/bin/mysqldump -u ${username} -p"${password}"  ${dbName} | sudo dd of="${FILENAME}.sql" && echo "DB EXPORTADA CON EXITO!" || echo "ERROR al exportar DB"
	echo "------COMPRIMIENDO------"
	sudo zip -r "${FILENAME}.zip" "wp-content/uploads" "${FILENAME}.sql" && echo "ARCHIVOS EXPORTADOS CON EXITO" || echo "ERROR al exportar ARCHIVOS"
	sudo mv "${FILENAME}.zip" "${BACKUP_DIR}"

done 


echo "------CAMBIANDO PERMISOS------" 
chmod -R 775 ${BACKUP_DIR}
chown -R ubuntu:www-data ${BACKUP_DIR}

echo "------REINICIANDO STACK ------" 
sudo su ubuntu -c 'sudo wo stack reload'

echo "LISTO!!!"